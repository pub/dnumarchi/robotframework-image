# Image OCI Robotframework

## Motivation

Des images [Robotframework](https://robotframework.org/) existent, voir par les [exemples documentés](https://docs.robotframework.org/docs/using_rf_in_ci_systems/docker).

* elles contiennent des librairies pas toujours utiles au projet de test, d'où une grande taille
* elles ne contiennent pas d'autres librairies parfois nécessaires (ex. robotframework-requests).

Ce [Dockerfile](Dockerfile) permet de construire une image OCI adaptée. 

## Contenu de l'image

L'image est basée sur l'[image Playwright](https://mcr.microsoft.com/en-us/product/playwright/about).

Les librairies Robotframework embarquées sont listées dans le fichier [requirements.txt](requirements.txt), adaptez les à votre besoin.
