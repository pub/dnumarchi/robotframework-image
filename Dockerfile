FROM mcr.microsoft.com/playwright:v1.44.1-jammy

USER root
RUN apt-get update \
  && apt-get install -y python3-pip \
  && rm -rf /var/lib/apt/lists/*

COPY requirements.txt .
RUN pip3 install -r requirements.txt --no-cache-dir --user 
RUN ~/.local/bin/rfbrowser init
ENV NODE_PATH=/usr/lib/node_modules
ENV PATH="/root/.local/bin:${PATH}"
